# User Registration, Authentication & Management GraphQL API

![GitHub](https://img.shields.io/github/license/zolamk/trust?style=for-the-badge) ![GitHub all releases](https://img.shields.io/github/downloads/zolamk/trust/total?style=for-the-badge)

Trust is an open source GraphQL API that can be used to handle user registration, authentication and management.

Trust documentation can be found at [https://trust.zelalem.me](https://trust.zelalem.me)

## Features

- Email/Phone Registration & Authentication
- Social Authentication (Google, Facebook, Github)
- Email/Phone Verification
- Email/SMS Message Templating
- Email/Phone Invitation
- Email/Phone Account Recovery
- Account Audit Logs
- Login Hooks
- Configurable Rate Limiting
- Configurable Account Lockout Policy
- Email/Phone/Password Validation Rules
- Various JWT Algorithms (HS256, HS384, HS512, RS256, RS384, RS512, ES256, E384, ES512, more coming soon)
- Admin Queries & Mutation For User Management
- Custom Data With Schema
- And more [checkout the docs](https://trust.zelalem.me)

## Upcoming Features

- 2FA
- EdDSA Alogrithm
- ...